class Code
  PEGS = Hash.new('')
  PEGG = {
    red: 'r',
    green: 'g',
    blue: 'b',
    yellow: 'y',
    orange: 'o',
    purple: 'p'
  }
  attr_accessor :pegs
  def initialize(options = [])
    if options == [] || options.length < 4
      raise ArgumentError
    else
      PEGS['color1'] = options[0]
      PEGS['color2'] = options[1]
      PEGS['color3'] = options[2]
      PEGS['color4'] = options[3]
      @pegs = [options[0],options[1],options[2],options[3]]
      #@PEGS = defaults.merge(options)
    end
  end

  def self.random
    #create new random code
    str = ""
    options = "rgbyop"
    (0..3).each do |num|
      str << PEGG.values[rand(6)]
    end
    random_non = Code.new(str)
    random_non
  end

  def [](idx)
    @pegs[idx]
  end

  def exact_matches(trial_code)
    (0..3).count{|num| trial_code[num]==pegs[num] }
  end

  def near_matches(trial_code)
    near_and_exact = 0
    (0..3).each do |num|
      if pegs.include?(trial_code[num])
        if num == 0
          near_and_exact += 1
        #prevent overcounting
        elsif !trial_code[0..num-1].include?(trial_code[num])
          near_and_exact += 1
        end
      end
    end
    if near_and_exact < exact_matches(trial_code)
      return 0
    else
      return near_and_exact-exact_matches(trial_code)
    end
  end

  def ==(other)
    if !other.is_a? Code
      return false
    else
      self.pegs == other.pegs
    end
  end

  #need to put self to be able to call without creating object
  def self.parse(str)

    str= str.downcase
    bleh = Code.new(str)
    if str.length != 4
      raise "Need to be length of 4!"
    elsif !str.chars.all?{|let| "rgbyop".include?(let)}
      raise "Non-valid letter found"
    else
      bleh
    end
  end

end

class Game
  attr_accessor :secret_code
  def initialize(code=Code.random)
    @secret_code = code
  end

  def get_guess
    puts "input your guess"
    #trial_code = gets.chomp
    Code.parse($stdin.gets.chomp)
  end

  def display_matches(trial_code)
    puts "you have #{@secret_code.near_matches(trial_code)} near_matches"
    puts "and #{@secret_code.exact_matches(trial_code)} exact_matches"
  end

  def self.play
    game = Game.new
    turns = 0
    trial_code = []
    while !game.won?(trial_code) && turns < 10
      trial_code = game.get_guess
      game.display_matches(trial_code)
    end
    if game.won?(trial_code)
      puts "Congrats, you guessed it in #{turns} trys"
    else
      puts "Good try, but you've run out of tries"
    end
  end

  def won?(trial_code)
    @secret_code == trial_code
  end
end
Game.play
